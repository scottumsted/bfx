package com.fedex.bfx.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fedex.bfx.model.BleActivity;

import android.content.Context;
import android.util.Log;


public class BfxResource {
	
	@SuppressWarnings("unused")
	private Context context;
	
	private static final String REST_URL = "http://ble.ezpz.gs";
	//private static final String REST_URL = "http://192.168.2.18:5006";
	
	public BfxResource(Context context){
		this.context = context;
	}
	
	public List<BleActivity> getBleActivities(){
		List<BleActivity> bleActivities = null;
		// TODO : Complete
		return bleActivities;
	}
	
	public int postBleActivity(BleActivity bleActivity){
		int activityId= -1;
		String url = REST_URL +"/activity.json";
		try{
			HttpContext context = new BasicHttpContext();
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 10000);
			HttpClient client = new DefaultHttpClient(httpParams);
			HttpPost post = new HttpPost(url);
			post.setEntity(new StringEntity(bleActivity.toJSON().toString(),"UTF-8"));
            post.setHeader("Content-type", "application/json; charset=utf-8");
			HttpResponse response = client.execute(post,context);
			String rJson = getResponseContentString(response);
			JSONObject mainJson = new JSONObject(rJson);
			if(mainJson.getBoolean("successful")){
				String idString = mainJson.getString("data");
				activityId = Integer.parseInt(idString);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch(Exception e){
			Log.e("getAllSince: ", e.getMessage());
			e.printStackTrace();
		}
		return activityId;
	}
	
	private String getResponseContentString(HttpResponse response){
		StringBuilder contentSb = new StringBuilder();
		String partial;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while((partial = reader.readLine())!=null){
				contentSb.append(partial);
			}
		} catch (IllegalStateException e) {
			Log.e("getResponseContentString: ",e.getMessage());
		} catch (IOException e) {
			Log.e("getResponseContentString: ",e.getMessage());
		} catch (Exception e){
			Log.e("getResponseContentString: ",e.getMessage());
		}
		return contentSb.toString();
	}
	
}
