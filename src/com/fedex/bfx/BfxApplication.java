package com.fedex.bfx;

import com.fedex.bfx.model.BfxDb;
import com.fedex.bfx.service.BfxResource;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

public class BfxApplication extends Application {

	private BfxDb model;
	private BfxResource resource;
	private String bleMode;
	
	@Override
	public void onCreate() {
		Log.v("onCreate","instantiating application context items");
		super.onCreate();
		model = new BfxDb(this);
		resource = new BfxResource(this);
		bleMode = "start";
	}

    public void showQuickToast(String message){
        Context context = getApplicationContext();
        CharSequence text = (CharSequence)message;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }

	public BfxDb getModel() {
		return model;
	}

	public String getBleMode() {
		return bleMode;
	}

	public void setBleMode(String bleMode) {
		this.bleMode = bleMode;
	}

	public BfxResource getResource() {
		return resource;
	}
	
}
