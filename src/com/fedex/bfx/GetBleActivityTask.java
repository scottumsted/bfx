package com.fedex.bfx;

import java.util.List;
import com.fedex.bfx.model.BleActivity;
import android.content.Context;
import android.os.AsyncTask;

public class GetBleActivityTask extends AsyncTask<BleActivity, Void, List<BleActivity>> {

	private Context applicationContext;
	
	public GetBleActivityTask(Context applicationContext){
		this.applicationContext = applicationContext;
	}
	
	@Override
	protected List<BleActivity> doInBackground(BleActivity... arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void onPostExecute(List<BleActivity> bleActivities) {
		for(BleActivity bleActivity : bleActivities){
			((BfxApplication) applicationContext).getModel().saveBleActivity(bleActivity);
		}
	}
}
