package com.fedex.bfx;

import java.util.ArrayList;
import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;

import com.fedex.bfx.model.BleActivity;
import com.fedex.bfx.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity  {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_fullscreen);

		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		final View contentView = findViewById(R.id.fullscreen_content);

		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(this, contentView,
				HIDER_FLAGS);
		mSystemUiHider.setup();
		mSystemUiHider
				.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
					// Cached values.
					int mControlsHeight;
					int mShortAnimTime;

					@Override
					@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
					public void onVisibilityChange(boolean visible) {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
							// If the ViewPropertyAnimator API is available
							// (Honeycomb MR2 and later), use it to animate the
							// in-layout UI controls at the bottom of the
							// screen.
							if (mControlsHeight == 0) {
								mControlsHeight = controlsView.getHeight();
							}
							if (mShortAnimTime == 0) {
								mShortAnimTime = getResources().getInteger(
										android.R.integer.config_shortAnimTime);
							}
							controlsView
									.animate()
									.translationY(visible ? 0 : mControlsHeight)
									.setDuration(mShortAnimTime);
						} else {
							// If the ViewPropertyAnimator APIs aren't
							// available, simply show or hide the in-layout UI
							// controls.
							controlsView.setVisibility(visible ? View.VISIBLE
									: View.GONE);
						}

						if (visible && AUTO_HIDE) {
							// Schedule a hide().
							delayedHide(AUTO_HIDE_DELAY_MILLIS);
						}
					}
				});

		// Set up the user interaction to manually show or hide the system UI.
		contentView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (TOGGLE_ON_CLICK) {
					mSystemUiHider.toggle();
				} else {
					mSystemUiHider.show();
				}
			}
		});

		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
		findViewById(R.id.action_button).setOnTouchListener(
				mDelayHideTouchListener);
		
		// if someone clicks the big button
		
		final ImageButton bigButton = (ImageButton)findViewById(R.id.content_image);
		bigButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				// disable button
				TextView subLabel = (TextView)findViewById(R.id.sub_content);
				if(((BfxApplication) getApplication()).getBleMode().equals("start")){
					subLabel.setText("Select from the\noptions menu");
				}else{
		            scanBle(false);
					subLabel.setText("Scanning for\npackages");
		    		goBle();
				}
			}
		});
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		delayedHide(100);
		setupBle();
	}

	/**
	 * Touch listener to use for in-layout UI controls to delay hiding the
	 * system UI. This is to prevent the jarring behavior of controls going away
	 * while interacting with activity UI.
	 */
	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (AUTO_HIDE) {
				delayedHide(AUTO_HIDE_DELAY_MILLIS);
			}
			return false;
		}
	};

	Handler mHideHandler = new Handler();
	Runnable mHideRunnable = new Runnable() {
		@Override
		public void run() {
			mSystemUiHider.hide();
		}
	};

	/**
	 * Schedules a call to hide() in [delay] milliseconds, canceling any
	 * previously scheduled calls.
	 */
	private void delayedHide(int delayMillis) {
		mHideHandler.removeCallbacks(mHideRunnable);
		mHideHandler.postDelayed(mHideRunnable, delayMillis);
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.bfx_options_menu, menu);
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	TextView mainLabel = (TextView)this.findViewById(R.id.fullscreen_content);
		TextView subLabel = (TextView)this.findViewById(R.id.sub_content);
		ImageView mainImage = (ImageView)this.findViewById(R.id.content_image);
		switch(item.getItemId()){
	    	case R.id.airplane:
	    		mainLabel.setText(R.string.airplane_label);
	    		subLabel.setText("Tap to begin\npackage scan");
	    		mainImage.setImageResource(R.drawable.plane_inverse);
	    		((BfxApplication) getApplication()).setBleMode("Airplane");
	    		break;
	    	case R.id.van:
	    		mainLabel.setText(R.string.van_label);
	    		subLabel.setText("Tap to begin\npackage scan");
	    		mainImage.setImageResource(R.drawable.van_inverse);
	    		((BfxApplication) getApplication()).setBleMode("Van");
	    		break;
	    	case R.id.quit:
	    		System.exit(0);
	    		break;
    	}
    	return true;
    }
    
    private void savePackage(String name){
    	// TODO : add location
		SaveBleActivityTask saveTask = new SaveBleActivityTask();
		BleActivity bleActivity = new BleActivity();
		bleActivity.setActivity("Scan package");
		bleActivity.setControl(((BfxApplication) getApplication()).getBleMode());
		bleActivity.setPayload("Package: "+name+", Tracking: 99999, Max Temp: 23, Max Acc: 1.5, Loc: 35N / 90W");
		bleActivity.setReceiptDate("0000000000");
		bleActivity.setPeripheral("Package");
		bleActivity.setSynched(false);
		saveTask.execute(bleActivity);
    }
    
    @SuppressWarnings("unused")
	private void showQuickToast(String message){
    	((BfxApplication)getApplication()).showQuickToast(message);
    }

    private class SaveBleActivityTask extends AsyncTask<BleActivity, Void, List<BleActivity>> {
    	@Override
    	protected List<BleActivity> doInBackground(BleActivity... activitiesArray) {
    		for(BleActivity activity: activitiesArray){
    			((BfxApplication) getApplication()).getModel().saveBleActivity(activity);
    		}
    		
    		List<BleActivity> postedActivities = new ArrayList<BleActivity>();
    		List<BleActivity> pendingActivities = new ArrayList<BleActivity>();
    		pendingActivities = ((BfxApplication) getApplication()).getModel().getUnsynchedBleActivities();
    		for(BleActivity activity: pendingActivities){
    			int sid = ((BfxApplication) getApplication()).getResource().postBleActivity(activity);
    			if(sid>0){
    				activity.setServiceId(sid);
    				activity.setSynched(true);
    				int recordsPosted= ((BfxApplication) getApplication()).getModel().updateBleActivity(activity);
    				if(recordsPosted>0){
    					postedActivities.add(activity);
    				}
    			}
    		}
    		return postedActivities;
    	}

    	@Override
    	protected void onPostExecute(List<BleActivity> bleActivities) {
    		if(bleActivities.size()>0){
    			((BfxApplication) getApplication()).showQuickToast("BLE Activity Posted");
    		}
    	}
    }

    //**************************
    // Bluetooth LE stuff here
    //**************************
    public void goBle(){
    	this.scanBle(true);
    }

    private BluetoothManager bluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private final static int REQUEST_ENABLE_BT = 1;
    
    private void setupBle(){
    	bluetoothManager =
        		(BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
    	mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }
    
    private boolean mScanning;
    private Handler mHandler;
    private static final long SCAN_PERIOD = 1800000; 
    
    private void scanBle(final boolean enable){
    	if(enable){
    		mHandler = new Handler();
    		mHandler.postDelayed(new Runnable(){

				@Override
				public void run() {
					mScanning = false;
					mBluetoothAdapter.stopLeScan(mLeScanCallback);
		    		stopScanningUi();
				}}, SCAN_PERIOD);
    		mScanning = true;
    		mBluetoothAdapter.startLeScan(mLeScanCallback);
    	} else {
    		mScanning = false;
    		mBluetoothAdapter.stopLeScan(mLeScanCallback);
    		stopScanningUi();
    	}
    }
    
    private void stopScanningUi(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
				TextView subLabel = (TextView)findViewById(R.id.sub_content);
				subLabel.setText("Scan\ncomplete"); //TODO add device name
            }
        });
    }
    
    //private LeDeviceListAdapter mLeDevideListAdapater;
    
    private LeScanCallback mLeScanCallback = new LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            //For the device we want, make a connection
            // TODO : get device name
        	//device.connectGatt(getApplication(), true, mGattCallback);
            // TODO: stop here on location since i don't have a real tag
            // if smarttag 
            // kill bluetooth close it
            // update subContent in a UI thread
            // construct and write event to ble.wuppsy.com
            // enable button
        	final String name = device.getName();
        	scanBle(false);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
    				TextView subLabel = (TextView)findViewById(R.id.sub_content);
    				subLabel.setText("Package located\n"+name); //TODO add device name
    				savePackage(name);
                }
            });
        }
    };
    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            //Connection established
            if (status == BluetoothGatt.GATT_SUCCESS
                    && newState == BluetoothProfile.STATE_CONNECTED) {
     
                //Discover services
                gatt.discoverServices();
     
            } else if (status == BluetoothGatt.GATT_SUCCESS
                    && newState == BluetoothProfile.STATE_DISCONNECTED) {
     
                //Handle a disconnect event
     
            }
        }
     
		@Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			Log.d("onServicesDiscovered","called");
			for(BluetoothGattService gattService: gatt.getServices()){
				String uuid = gattService.getUuid().toString();
				Log.d("onServicesDiscovered","UUID:"+uuid);
				if(uuid.indexOf("6b36")>=0){
					Log.d("onServicesDiscovered","**** Matching UUID:"+uuid);
				}
				for(BluetoothGattCharacteristic gattCharacteristic: gattService.getCharacteristics()){
					//gatt.readCharacteristic(gattCharacteristic);
				}
			}
		}
		
		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			Log.d("onCharacteristicRead","called");
			String descriptorValue = characteristic.getDescriptor(characteristic.getUuid()).getCharacteristic().getStringValue(0);
			Log.d("onCharacteristicRead",descriptorValue);
		}
    };
    
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
   
}

