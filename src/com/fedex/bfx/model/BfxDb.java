package com.fedex.bfx.model;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * NexusTravelerDb defines the sqlite db and associated access methods.
 * @author 814931 - Scott Umsted
 *
 */
public class BfxDb extends SQLiteOpenHelper {

	protected static final int DB_VERSION=7;
	protected static final String DB_NAME="BfxDb";
	
	protected static final String BLE_ACTIVITY_DROP_SQL = "drop table ble_activity ";
	protected static final String BLE_ACTIVITY_CREATE_SQL = "create table ble_activity(" +
			"peripheral_activity_id integer primary key autoincrement, " +
			"activity text, " +
			"control text, " +
			"peripheral text, " +
			"payload text, " +
			"receipt_date text, " +
			"synched false, " +
			"service_id int ) ";
	
	public BfxDb(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// DDL
		try{
			db.execSQL(BLE_ACTIVITY_CREATE_SQL);
		}catch(Exception e){
			Log.v("onCreate: DDL: ",e.getMessage());
		}
		
		// DML
		try{
			dml(db);
		}catch(Exception e){
			Log.v("onCreate: DML: ",e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		// DDL
		try{
			db.execSQL(BLE_ACTIVITY_DROP_SQL);
			db.execSQL(BLE_ACTIVITY_CREATE_SQL);
		}catch(Exception e){
			Log.v("onUpgrade: DDL: ",e.getMessage());
		}
		
		// DML
		try{
			dml(db);
		}catch(Exception e){
			Log.v("onCreate: DML: ",e.getMessage());
		}
	}

	private void dml(SQLiteDatabase db){
		// delete data
		try{
			db.delete("ble_activity", "1", null);
		}catch(Exception e){
			Log.v("dml: delete: ",e.getMessage());
		}
	}
	
	public List<BleActivity> getBleActivities(){
		List<BleActivity> bleActivities = new ArrayList<BleActivity>();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = getReadableDatabase();
			cursor = db.query("ble_activity", null, null, null, null, null, null);
			cursor.moveToFirst();
			while(!cursor.isAfterLast()){
				BleActivity bleActivity = new BleActivity();
				bleActivity.setPeripheralActivityId(cursor.getInt(cursor.getColumnIndex("peripheral_activity_id")));
				bleActivity.setActivity(cursor.getString(cursor.getColumnIndex("activity")));
				bleActivity.setControl(cursor.getString(cursor.getColumnIndex("control")));
				bleActivity.setPeripheral(cursor.getString(cursor.getColumnIndex("peripheral")));
				bleActivity.setPayload(cursor.getString(cursor.getColumnIndex("payload")));
				bleActivity.setReceiptDate(cursor.getString(cursor.getColumnIndex("receipt_date")));
				bleActivity.setSynched(cursor.getString(cursor.getColumnIndex("synched")).equals("true"));
				bleActivity.setServiceId(cursor.getInt(cursor.getColumnIndex("service_id")));
				bleActivities.add(bleActivity);
				cursor.moveToNext();
			}
		}catch(Exception e){
			Log.v("getUnsynchedBleActivities: ",e.getMessage());
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}
		return bleActivities;
	}
	
	public List<BleActivity> getUnsynchedBleActivities(){
		List<BleActivity> bleActivities = new ArrayList<BleActivity>();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = getReadableDatabase();
			cursor = db.query("ble_activity", null, "synched=?", new String[]{"false"}, null, null, null);
			cursor.moveToFirst();
			while(!cursor.isAfterLast()){
				BleActivity bleActivity = new BleActivity();
				bleActivity.setPeripheralActivityId(cursor.getInt(cursor.getColumnIndex("peripheral_activity_id")));
				bleActivity.setActivity(cursor.getString(cursor.getColumnIndex("activity")));
				bleActivity.setControl(cursor.getString(cursor.getColumnIndex("control")));
				bleActivity.setPeripheral(cursor.getString(cursor.getColumnIndex("peripheral")));
				bleActivity.setPayload(cursor.getString(cursor.getColumnIndex("payload")));
				bleActivity.setReceiptDate(cursor.getString(cursor.getColumnIndex("receipt_date")));
				bleActivity.setSynched(cursor.getString(cursor.getColumnIndex("synched")).equals("true"));
				bleActivity.setServiceId(cursor.getInt(cursor.getColumnIndex("service_id")));
				bleActivities.add(bleActivity);
				cursor.moveToNext();
			}
		}catch(Exception e){
			Log.v("getUnsynchedBleActivities: ",e.getMessage());
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}
		return bleActivities;
	}
	
	public List<BleActivity> getBleActivity(int id){
		List<BleActivity> bleActivities = new ArrayList<BleActivity>();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try{
			db = getReadableDatabase();
			cursor = db.query("ble_activity", null, "peripheral_activity_id=?", new String[]{Integer.toString(id)}, null, null, null);
			cursor.moveToFirst();
			while(!cursor.isAfterLast()){
				BleActivity bleActivity = new BleActivity();
				bleActivity.setPeripheralActivityId(cursor.getInt(cursor.getColumnIndex("peripheral_activity_id")));
				bleActivity.setActivity(cursor.getString(cursor.getColumnIndex("activity")));
				bleActivity.setControl(cursor.getString(cursor.getColumnIndex("control")));
				bleActivity.setPeripheral(cursor.getString(cursor.getColumnIndex("peripheral")));
				bleActivity.setPayload(cursor.getString(cursor.getColumnIndex("payload")));
				bleActivity.setReceiptDate(cursor.getString(cursor.getColumnIndex("receipt_date")));
				bleActivity.setSynched(cursor.getString(cursor.getColumnIndex("synched")).equals("true"));
				bleActivity.setServiceId(cursor.getInt(cursor.getColumnIndex("service_id")));
				bleActivities.add(bleActivity);
				cursor.moveToNext();
			}
		}catch(Exception e){
			Log.v("getUnsynchedBleActivities: ",e.getMessage());
		}finally{
			try{cursor.close();db.close();}catch(Exception e){}
		}
		return bleActivities;
	}

	public int saveBleActivity(BleActivity bleActivity){
		int count = 0;
		SQLiteDatabase db = null;
		try{
			db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("peripheral", bleActivity.getPeripheral());
			values.put("control", bleActivity.getControl());
			values.put("activity", bleActivity.getActivity());
			values.put("payload", bleActivity.getPayload());
			values.put("receipt_date", bleActivity.getReceiptDate());
			values.put("synched", bleActivity.isSynched()?"true":"false");
			count = (int) db.insert("ble_activity", null, values);
		}catch(Exception e){
			Log.v("saveBleActivity", e.getMessage());
		}finally{
			try{db.close();}catch(Exception e){}
		}
		return count;
	}

	public int updateBleActivity(BleActivity bleActivity){
		int count = 0;
		SQLiteDatabase db = null;
		try{
			db = getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put("synched", bleActivity.isSynched()?"true":"false");
			values.put("service_id", bleActivity.getServiceId());
			count = db.update("ble_activity", values, "peripheral_activity_id=?", 
					new String[]{Integer.toString(bleActivity.getPeripheralActivityId())});
		}catch(Exception e){
			Log.v("saveBleActivity", e.getMessage());
		}finally{
			try{db.close();}catch(Exception e){}
		}
		return count;
	}
}
