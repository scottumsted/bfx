package com.fedex.bfx.model;

import org.json.JSONException;
import org.json.JSONObject;

public class BleActivity {

	private int peripheralActivityId; 
	private String activity;
	private String control;
	private String peripheral;
	private String payload;
	private String receiptDate;
	private boolean synched;
	private int serviceId;
	
	public String toJSON(){
	    JSONObject jsonObject= new JSONObject();
	    try {
	        jsonObject.put("server_activity_id", getPeripheralActivityId());
	        jsonObject.put("activity", getActivity());
	        jsonObject.put("client", getControl());
	        jsonObject.put("server", getPeripheral());
	        jsonObject.put("payload", getPayload());
	        jsonObject.put("receipt_date", getReceiptDate());
	        return jsonObject.toString();
	    } catch (JSONException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        return "";
	    }
	}

	public BleActivity() {
		// TODO Auto-generated constructor stub
	}
	
	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getPeripheral() {
		return peripheral;
	}

	public void setPeripheral(String peripheral) {
		this.peripheral = peripheral;
	}

	public int getPeripheralActivityId() {
		return peripheralActivityId;
	}

	public void setPeripheralActivityId(int peripheralActivityId) {
		this.peripheralActivityId = peripheralActivityId;
	}

	public String getControl() {
		return control;
	}

	public void setControl(String control) {
		this.control = control;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	public boolean isSynched() {
		return synched;
	}

	public void setSynched(boolean synched) {
		this.synched = synched;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getReceiptDatePrettyString(){
		return getPrettyDatetime(getReceiptDate());
	}

	private String getPrettyDatetime(String d){
		String prettyDate = "";
		if(d!=null && d.length()==14){
			prettyDate = d.substring(4, 6)+"/"+d.substring(6, 8)+"/"+d.substring(0,4)+" "+d.substring(8,10)+":"+d.substring(10,12)+":"+d.substring(12,14);
		}
		return prettyDate;
	}
}
